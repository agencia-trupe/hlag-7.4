import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(document).ready(function () {
    $("select[name=lang]").change(function (e) {
        const route = $(this).data("route");
        const selection = $(this).val();

        location.href = `${route}/${selection}`;
    });

    $(".datepicker").datepicker({
        autoHide: true,
        language: "pt-BR",
        format: "dd/mm",
        template: `
            <div class="datepicker-container">
                <div class="datepicker-panel" data-view="years picker">
                    <ul>
                        <li data-view="month prev">&lsaquo;</li>
                        <li data-view="month current"></li>
                        <li data-view="month next">&rsaquo;</li>
                    </ul>
                    <ul data-view="week"></ul>
                    <ul data-view="days"></ul>
                </div>
                <div class="datepicker-panel" data-view="months picker">
                    <ul>
                        <li data-view="month prev">&lsaquo;</li>
                        <li data-view="month current"></li>
                        <li data-view="month next">&rsaquo;</li>
                    </ul>
                    <ul data-view="week"></ul>
                    <ul data-view="days"></ul>
                </div>
                <div class="datepicker-panel" data-view="days picker">
                    <ul>
                        <li data-view="month prev">&lsaquo;</li>
                        <li data-view="month current"></li>
                        <li data-view="month next">&rsaquo;</li>
                    </ul>
                    <ul data-view="week"></ul>
                    <ul data-view="days"></ul>
                </div>
            </div>
        `,
    });

    $("form[method=GET]").submit(function () {
        $(this)
            .find(":input")
            .each(function () {
                if (!$(this).val()) $(this).attr("disabled", true);
            });
    });

    $(".filtros select")
        .change(function (e) {
            if ($(this).val() == "") {
                $(this).addClass("placeholder");
            } else {
                $(this).removeClass("placeholder");
            }
        })
        .trigger("change");

    $(".datepicker").keyup(function () {
        if (/[^0-9|\/]/g.test(this.value)) {
            this.value = this.value.replace(/[^0-9|\/]/g, "");
        }
    });

    $(".consulta table").stickyTableHeaders();

    $(".consulta .table-wrapper").on("scroll", () => {
        $(window).trigger("resize.stickyTableHeaders");
    });

    $(".filtros select[name=porto]").selectize();
    $(".filtros select[name=navio]").selectize();
    $(".filtros select[name=servico]").selectize();
});
