<?php

return [

    'nav' => [
        'programacao' => 'Ship Programming',
        'servicos'    => 'Services',
        'terminais'   => 'Berthing Terminals',
        'contatos'    => 'Contacts',
    ],

    'programacao' => [
        'nenhuma'                  => 'No routes announced at the moment.',
        'nenhum-resultado'         => 'No results found.',
        'consulte'                 => 'See available routes',
        'selecione'                => 'Select the filters for your query:',
        'divulgado'                => 'Published in:',
        'porto'                    => 'Harbor',
        'navio'                    => 'Ship',
        'viagem'                   => 'Travel',
        'servico'                  => 'Service',
        'previsao_atracacao'       => 'Mooring Forecast',
        'previsao_saida'           => 'Output Forecast',
        'deadline_dca'             => 'DCA Deadline',
        'deadline_mdgf'            => 'MDGF Deadline',
        'deadline_draft'           => 'Draft Deadline',
        'deadline_carga_liberacao' => 'Loading/Release Deadline',
        'deadline_vgm'             => 'VGM Deadline',
        'terminal'                 => 'Terminal',
        'pernada'                  => 'Pernada',
        'inicio'                   => 'start',
        'fim'                      => 'end',
        'prazos'                   => 'Deadlines',
        'deadlines'                => 'Deadlines',
        'dca'                      => 'DCA',
        'mdgf'                     => 'MDGF',
        'draft'                    => 'Draft',
        'carga_liberacao'          => 'Load/Release',
        'vgm'                      => 'VGM',
        'consultar'                => 'Consult',
        'opcoes-consulta'          => 'Query Options',
        'exportar-xls'             => 'Export XLS',
        'exportar-pdf'             => 'Export PDF',
    ],

    '404'          => 'Page not found',
    'fale-conosco' => 'Contact us:',

];
