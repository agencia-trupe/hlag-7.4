<?php

return [

    'nav' => [
        'programacao' => 'Programación de Barcos',
        'servicos'    => 'Servicios',
        'terminais'   => 'Terminales de Atraque',
        'contatos'    => 'Contactos',
    ],

    'programacao' => [
        'nenhuma'                  => 'No hay rutas anunciadas por el momento.',
        'nenhum-resultado'         => 'Ningún resultado encontrado.',
        'consulte'                 => 'Ver rutas disponibles',
        'selecione'                => 'Selecciona filtros para tu consulta:',
        'divulgado'                => 'Publicado en:',
        'porto'                    => 'Puerto',
        'navio'                    => 'Barco',
        'viagem'                   => 'Viaje',
        'servico'                  => 'Servicios',
        'previsao_atracacao'       => 'Pronóstico Amarre',
        'previsao_saida'           => 'Pronóstico de Salida',
        'deadline_dca'             => 'Deadline DCA',
        'deadline_mdgf'            => 'Deadline MDGF',
        'deadline_draft'           => 'Deadline Draft',
        'deadline_carga_liberacao' => 'Deadline Cargar/Liberar.',
        'deadline_vgm'             => 'Deadline VGM',
        'terminal'                 => 'Terminal',
        'pernada'                  => 'Pernada',
        'inicio'                   => 'comienzo',
        'fim'                      => 'el fin',
        'prazos'                   => 'Plazos',
        'deadlines'                => 'Deadlines',
        'dca'                      => 'DCA',
        'mdgf'                     => 'MDGF',
        'draft'                    => 'Draft',
        'carga_liberacao'          => 'Cargar/Liberar',
        'vgm'                      => 'VGM',
        'consultar'                => 'Consultar',
        'opcoes-consulta'          => 'Opciones de consulta',
        'exportar-xls'             => 'Exportar XLS',
        'exportar-pdf'             => 'Exportar PDF',
    ],

    '404'          => 'Página no encontrada',
    'fale-conosco' => 'Hable con nosotros:',

];
