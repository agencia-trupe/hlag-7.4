<?php

return [

    'nav' => [
        'programacao' => 'Programação de Navios',
        'servicos'    => 'Serviços',
        'terminais'   => 'Terminais de Atracação',
        'contatos'    => 'Contatos',
    ],

    'programacao' => [
        'nenhuma'                  => 'Nenhuma rota divulgada no momento.',
        'nenhum-resultado'         => 'Nenhum resultado encontrado.',
        'consulte'                 => 'Consulte as rotas disponíveis',
        'selecione'                => 'Selecione os filtros para a sua consulta:',
        'divulgado'                => 'Divulgado em:',
        'porto'                    => 'Porto',
        'navio'                    => 'Navio',
        'viagem'                   => 'Viagem',
        'servico'                  => 'Serviço',
        'previsao_atracacao'       => 'Previsão de Atracação',
        'previsao_saida'           => 'Previsão de Saída',
        'deadline_dca'             => 'Deadline de DCA',
        'deadline_mdgf'            => 'Deadline de MDGF',
        'deadline_draft'           => 'Deadline de Draft',
        'deadline_carga_liberacao' => 'Deadline de Carga/Liber.',
        'deadline_vgm'             => 'Deadline de VGM',
        'terminal'                 => 'Terminal',
        'pernada'                  => 'Pernada',
        'inicio'                   => 'início',
        'fim'                      => 'fim',
        'prazos'                   => 'Prazos',
        'deadlines'                => 'Deadlines',
        'dca'                      => 'DCA',
        'mdgf'                     => 'MDGF',
        'draft'                    => 'Draft',
        'carga_liberacao'          => 'Carga/Liberação',
        'vgm'                      => 'VGM',
        'consultar'                => 'Consultar',
        'opcoes-consulta'          => 'Opções de Consulta',
        'exportar-xls'             => 'Exportar XLS',
        'exportar-pdf'             => 'Exportar PDF',
    ],

    '404'          => 'Página não encontrada',
    'fale-conosco' => 'Fale Conosco:',

];
