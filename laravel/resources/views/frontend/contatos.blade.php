@extends('frontend.layout.template')

@section('content')

<div class="main">
    <div class="center">
        <h1>{{ trans('frontend.nav.contatos') }}</h1>

        @foreach($divisoes as $divisao)
        <div class="contatos-divisao">
            <h2>{{ $divisao->{trans('database.titulo')} }}</h2>
            <h3>{{ $divisao->{trans('database.telefones')} }}</h3>
            <div class="emails">
                @foreach($divisao->emails as $email)
                <div class="email">
                    <span>{{ $email->{trans('database.titulo')} }}:</span>
                    <a href="mailto:{{ $email->email }}">
                        {{ $email->email }}
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection