<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
    <style>
        @page { margin: 0in; }
        body { padding: 30px; }
        img { display: block; margin-bottom: 30px; }
        table {
            width: 100%;
            font-family: 'Open Sans', Helvetica, Arial, sans-serif;
            font-weight: 400;
            font-style: normal;
            border-collapse: collapse
        }
        table th {
            font-size: 8px;
            font-weight: 700;
            text-transform: uppercase;
            color: #23467d;
            text-align: left;
            vertical-align: top;
            padding-right: 10px;
            padding-bottom: 5px
        }
        table th:last-child { padding-right: 0; }
        table td {
            font-size: 10px;
            color: #222;
            border-top: 1px solid #ccc;
            padding: 10px 0;
            padding-right: 10px
        }
        table td:nth-child(3),
        table td:nth-child(4),
        table td:nth-child(5),
        table td:nth-child(6),
        table td:nth-child(7),
        table td:nth-child(8),
        table td:nth-child(9),
        table td:nth-child(10),
        table td:nth-child(11) { white-space: nowrap; }
        table td:nth-child(13) { padding-right: 0; text-align: center; }
        table tr:last-child td { border-bottom: 1px solid #ccc; }
    </style>
</head>
<body>
    <img src="{{ public_path('/assets/img/layout/marca-hapaglloyd.png') }}">
    <table>
        <thead>
            <tr>
                <th>{{ trans('frontend.programacao.porto') }}</th>
                <th>{{ trans('frontend.programacao.navio') }}</th>
                <th>{{ trans('frontend.programacao.viagem') }}</th>
                <th>{{ trans('frontend.programacao.servico') }}</th>
                <th>{{ trans('frontend.programacao.previsao_atracacao') }}</th>
                <th>{{ trans('frontend.programacao.previsao_saida') }}</th>
                <th>{{ trans('frontend.programacao.deadline_dca') }}</th>
                <th>{{ trans('frontend.programacao.deadline_mdgf') }}</th>
                <th>{{ trans('frontend.programacao.deadline_draft') }}</th>
                <th>{{ trans('frontend.programacao.deadline_carga_liberacao') }}</th>
                <th>{{ trans('frontend.programacao.deadline_vgm') }}</th>
                <th>{{ trans('frontend.programacao.terminal') }}</th>
                <th>{{ trans('frontend.programacao.pernada') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($resultados as $resultado)
            <tr>
                <td>{{ $resultado->porto }}</td>
                <td>{{ $resultado->navio }}</td>
                <td>{{ $resultado->viagem }}</td>
                <td>{{ $resultado->servico }}</td>
                <td>{{ $resultado->previsao_atracacao->format('d/m') }}</td>
                <td>{{ $resultado->previsao_saida->format('d/m') }}</td>
                <td>{{ $resultado->deadline_dca->format('d/m H:i') }}</td>
                <td>{{ $resultado->deadline_mdgf->format('d/m H:i') }}</td>
                <td>{{ $resultado->deadline_draft->format('d/m H:i') }}</td>
                <td>{{ $resultado->deadline_carga_liberacao->format('d/m H:i') }}</td>
                <td>{{ $resultado->deadline_vgm->format('d/m H:i') }}</td>
                <td>{{ $resultado->terminal }}</td>
                <td>{{ $resultado->pernada }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
