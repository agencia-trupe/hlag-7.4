@extends('frontend.layout.template')

@section('content')

    <div class="main programacao">
        <div class="center">
            <h1>{{ trans('frontend.programacao.consulte') }}</h1>

            <div class="selecione-divulgacao">
                <p>{{ trans('frontend.programacao.selecione') }}</p>
                <p>
                    {{ trans('frontend.programacao.divulgado') }}
                    {{ $programacao->last()->created_at->format('d/m/Y') }}
                </p>
            </div>

            <div class="opcoes-consulta">
                <a href="{{ route('home', app()->getLocale()) }}">
                    {{ trans('frontend.programacao.opcoes-consulta') }}
                </a>
            </div>
        </div>
    </div>

    <div class="consulta">
        <div class="center">
            @if(count($resultados))
            <div class="table-wrapper">
                <table>
                    <thead>
                        <tr>
                            <th>{{ trans('frontend.programacao.porto') }}</th>
                            <th>{{ trans('frontend.programacao.navio') }}</th>
                            <th>{{ trans('frontend.programacao.viagem') }}</th>
                            <th>{{ trans('frontend.programacao.servico') }}</th>
                            <th>{{ trans('frontend.programacao.previsao_atracacao') }}</th>
                            <th>{{ trans('frontend.programacao.previsao_saida') }}</th>
                            <th>{{ trans('frontend.programacao.deadline_dca') }}</th>
                            <th>{{ trans('frontend.programacao.deadline_mdgf') }}</th>
                            <th>{{ trans('frontend.programacao.deadline_draft') }}</th>
                            <th>{{ trans('frontend.programacao.deadline_carga_liberacao') }}</th>
                            <th>{{ trans('frontend.programacao.deadline_vgm') }}</th>
                            <th>{{ trans('frontend.programacao.terminal') }}</th>
                            <th>{{ trans('frontend.programacao.pernada') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($resultados as $resultado)
                        <tr>
                            <td>{{ $resultado->porto }}</td>
                            <td>{{ $resultado->navio }}</td>
                            <td>{{ $resultado->viagem }}</td>
                            <td>{{ $resultado->servico }}</td>
                            <td>{{ $resultado->previsao_atracacao->format('d/m') }}</td>
                            <td>{{ $resultado->previsao_saida->format('d/m') }}</td>
                            <td>{{ $resultado->deadline_dca->format('d/m H:i') }}</td>
                            <td>{{ $resultado->deadline_mdgf->format('d/m H:i') }}</td>
                            <td>{{ $resultado->deadline_draft->format('d/m H:i') }}</td>
                            <td>{{ $resultado->deadline_carga_liberacao->format('d/m H:i') }}</td>
                            <td>{{ $resultado->deadline_vgm->format('d/m H:i') }}</td>
                            <td>{{ $resultado->terminal }}</td>
                            <td>{{ $resultado->pernada }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="exportar">
                @php 
                $extension = implode('-',array_merge(['extension' => 'xls'], $_GET)); 
                $serializeXls = serialize(array_merge(['xls'], $_GET));
                $serializePdf = serialize(array_merge(['pdf'], $_GET));
                @endphp
                <form action="{{ route('consulta.exportacao', ['lang' => app()->getLocale(), 'extension' => 'xls']) }}" method="get">
                    @foreach($_GET as $key => $value)
                    <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                    @endforeach
                    <button type="submit" class="exportar-xls">{{ trans('frontend.programacao.exportar-xls') }}</button>
                </form>
                <form action="{{ route('consulta.exportacao', ['lang' => app()->getLocale(), 'extension' => 'pdf']) }}" method="get">
                    @foreach($_GET as $key => $value)
                    <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                    @endforeach
                    <button type="submit" class="exportar-pdf">{{ trans('frontend.programacao.exportar-pdf') }}</button>
                </form>
            </div>
            @else
            <p>{{ trans('frontend.programacao.nenhum-resultado') }}</p>
            @endif
        </div>
    </div>

@endsection
