@extends('frontend.layout.template')

@section('content')

<div class="main programacao">
    <div class="center">
        <h1>{{ trans('frontend.programacao.consulte') }}</h1>

        @if(! count($programacao))
        <p>{{ trans('frontend.programacao.nenhuma') }}</p>
        @else
        <div class="selecione-divulgacao">
            <p>{{ trans('frontend.programacao.selecione') }}</p>
            <p>
                {{ trans('frontend.programacao.divulgado') }}
                {{ $programacao->last()->created_at->format('d/m/Y') }}
            </p>
        </div>

        <form action="{{ route('consulta', app()->getLocale()) }}" method="GET" class="filtros">
            <div class="left">
                <select name="porto">
                    <option value="">{{ trans('frontend.programacao.porto') }}</option>
                    @foreach($portos as $porto)
                    <option value="{{ $porto['sigla'] }}" @if(request('porto')==$porto) selected @endif>{{ $porto['sigla'] }} | {{ $porto['titulo'] }}</option>
                    @endforeach
                </select>
                <select name="navio">
                    <option value="">{{ trans('frontend.programacao.navio') }}</option>
                    @foreach($navios as $navio)
                    <option value="{{ $navio }}" @if(request('navio')==$navio) selected @endif>{{ $navio }}</option>
                    @endforeach
                </select>
                <select name="servico">
                    <option value="">{{ trans('frontend.programacao.servico') }}</option>
                    @foreach($servicos as $servico)
                    <option value="{{ $servico }}" @if(request('servico')==$servico) selected @endif>{{ $servico }}</option>
                    @endforeach
                </select>
            </div>
            <div class="right">
                <div class="row">
                    <label>{{ trans('frontend.programacao.previsao_atracacao') }}:</label>
                    <input type="text" name="previsao_atracacao_inicio" class="datepicker" placeholder="{{ trans('frontend.programacao.inicio') }}" autocomplete="off" maxlength="5" value="{{ request('previsao_atracacao_inicio') }}">
                    <input type="text" name="previsao_atracacao_fim" class="datepicker" placeholder="{{ trans('frontend.programacao.fim') }}" autocomplete="off" maxlength="5" value="{{ request('previsao_atracacao_fim') }}">
                </div>
                <div class="row">
                    <label>{{ trans('frontend.programacao.previsao_saida') }}:</label>
                    <input type="text" name="previsao_saida_inicio" class="datepicker" placeholder="{{ trans('frontend.programacao.inicio') }}" autocomplete="off" maxlength="5" value="{{ request('previsao_saida_inicio') }}">
                    <input type="text" name="previsao_saida_fim" class="datepicker" placeholder="{{ trans('frontend.programacao.fim') }}" autocomplete="off" maxlength="5" value="{{ request('previsao_saida_fim') }}">
                </div>
                <div class="row">
                    <label>{{ trans('frontend.programacao.prazos') }}:</label>
                    <select name="deadline">
                        <option value="">Deadlines</option>
                        @foreach(['dca', 'mdgf', 'draft', 'carga_liberacao', 'vgm'] as $dl)
                        <option value="{{ $dl }}" @if(request('deadline')==$dl) selected @endif>{{ trans('frontend.programacao.'.$dl) }}</option>
                        @endforeach
                    </select>
                    <input type="text" name="deadline_inicio" class="datepicker" placeholder="{{ trans('frontend.programacao.inicio') }}" autocomplete="off" maxlength="5" value="{{ request('deadline_inicio') }}">
                    <input type="text" name="deadline_fim" class="datepicker" placeholder="{{ trans('frontend.programacao.fim') }}" autocomplete="off" maxlength="5" value="{{ request('deadline_fim') }}">
                </div>
            </div>
            <div class="submit">
                <input type="submit" value="{{ trans('frontend.programacao.consultar') }}">
            </div>
        </form>
        @endif
    </div>
</div>

@endsection