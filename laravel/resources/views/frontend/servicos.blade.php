@extends('frontend.layout.template')

@section('content')

<div class="main">
    <div class="center">
        <h1>{{ trans('frontend.nav.servicos') }}</h1>

        <div class="links">
            @foreach($links as $link)
            <a href="{{ $link->link }}" target="_blank">
                {{ $link->{trans('database.titulo')} }}
            </a>
            @endforeach
        </div>
    </div>
</div>

@endsection