<header>
    <div class="center">
        <nav>
            @include('frontend.layout.nav')
            <!-- <div class="lang-wrapper">
                <a href="{{ route('home', app()->getLocale()) }}">Home</a>
                <select name="lang" data-route="{{ route('lang') }}">
                    @foreach(['pt', 'en', 'es'] as $lang)
                    <option value="{{ $lang }}" @if(app()->getLocale() == $lang) selected @endif
                        >
                        {{ strtoupper($lang) }}
                    </option>
                    @endforeach
                </select>
            </div> -->
        </nav>
    </div>
</header>