<a href="{{ route('home', app()->getLocale()) }}" @if(Tools::routeIs(['home', 'consulta'])) class="active" @endif>{{ trans('frontend.nav.programacao') }}</a>
<a href="{{ route('servicos', app()->getLocale()) }}" @if(Tools::routeIs('servicos')) class="active" @endif>{{ trans('frontend.nav.servicos') }}</a>
<a href="{{ route('terminais', app()->getLocale()) }}" @if(Tools::routeIs('terminais')) class="active" @endif>{{ trans('frontend.nav.terminais') }}</a>
<a href="{{ route('contatos', app()->getLocale()) }}" @if(Tools::routeIs('contatos')) class="active" @endif>{{ trans('frontend.nav.contatos') }}</a>
