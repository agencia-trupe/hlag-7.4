@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<a href="{{ route('painel.programacao.index') }}" title="Voltar para Programação" class="btn btn-secondary mb-3 col-2" style="width: 25%; font-size:14px">
    &larr; Voltar para Programação</a>

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">PORTOS</h2>

    <a href="{{ route('painel.portos.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Porto
    </a>
</legend>


@if(!count($portos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable">
        <thead>
            <tr>
                <th scope="col">Sigla</th>
                <th scope="col">Título</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($portos as $porto)
            <tr id="{{ $porto->id }}">
                <td>{{ $porto->sigla }}</td>
                <td>{{ $porto->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.portos.destroy', $porto->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.portos.edit', $porto->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection