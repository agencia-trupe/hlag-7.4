@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PORTOS |</small> Editar Porto</h2>
</legend>

{!! Form::model($porto, [
'route' => ['painel.portos.update', $porto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.portos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection