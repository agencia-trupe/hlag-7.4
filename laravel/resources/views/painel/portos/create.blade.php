@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>PORTOS |</small> Adicionar Porto</h2>
</legend>

{!! Form::open(['route' => 'painel.portos.store', 'files' => true]) !!}

@include('painel.portos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection