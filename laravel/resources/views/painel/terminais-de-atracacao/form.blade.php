@include('painel.layout.flash')

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_pt', 'Título [PT]') !!}
        {!! Form::text('titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_en', 'Título [EN]') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_es', 'Título [ES]') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('link', 'Link') !!}
    @if(isset($terminal))
    <p>
        <a href="{{ Tools::parseLink($terminal->link) }}" target="_blank">
            {{ $terminal->link }}
        </a>
    </p>
    @endif
    {!! Form::text('link', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.terminais-de-atracacao.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>