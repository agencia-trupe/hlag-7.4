@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>TERMINAIS DE ATRACAÇÃO |</small> Editar Terminal</h2>
</legend>

{!! Form::model($terminal, [
'route' => ['painel.terminais-de-atracacao.update', $terminal->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.terminais-de-atracacao.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection