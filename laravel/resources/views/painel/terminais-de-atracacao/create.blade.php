@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>TERMINAIS DE ATRACAÇÃO |</small> Adicionar Terminal</h2>
</legend>

{!! Form::open(['route' => 'painel.terminais-de-atracacao.store', 'files' => true]) !!}

@include('painel.terminais-de-atracacao.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection