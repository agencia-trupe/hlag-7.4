@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">CONFIGURAÇÕES</h2>
</legend>

{!! Form::model($configuracao, [
'route' => ['painel.configuracoes.update', $configuracao->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.configuracoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection