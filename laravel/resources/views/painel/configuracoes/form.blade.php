@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, ['class' => 'form-control input-text']) !!}
</div>
<div class="mb-3 col-12 col-md-12">
    {!! Form::label('description', 'Description') !!}
    {!! Form::text('description', null, ['class' => 'form-control input-text']) !!}
</div>
<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('keywords', 'Keywords') !!}
        {!! Form::text('keywords', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('analytics', 'Código Analytics') !!}
        {!! Form::text('analytics', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('imagem_de_compartilhamento', 'Imagem de compartilhamento') !!}
    @if($configuracao->imagem_de_compartilhamento)
    <img src="{{ url('assets/img/'.$configuracao->imagem_de_compartilhamento) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_de_compartilhamento', ['class' => 'form-control']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.configuracoes.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>