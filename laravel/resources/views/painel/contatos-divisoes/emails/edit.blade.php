@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>Contatos Divisão: {{ $divisao->titulo_pt }} |</small> Editar E-mail</h2>
</legend>

{!! Form::model($email, [
'route' => ['painel.contatos.divisoes.emails.update', $divisao->id, $email->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.contatos-divisoes.emails.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection