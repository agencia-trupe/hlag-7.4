@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>Contatos Divisão: {{ $divisao->titulo_pt }} |</small> Adicionar E-mail</h2>
</legend>

{!! Form::open(['route' => ['painel.contatos.divisoes.emails.store', $divisao->id], 'files' => true]) !!}

@include('painel.contatos-divisoes.emails.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection