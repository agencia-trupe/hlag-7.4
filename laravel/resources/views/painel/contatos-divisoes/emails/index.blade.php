@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<a href="{{ route('painel.contatos.divisoes.index') }}" title="Voltar para Divisões" class="btn btn-secondary mb-3 col-2">
    &larr; Voltar para Divisões</a>

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0"><small>Contatos Divisão: {{ $divisao->titulo_pt }} |</small> E-mails</h2>

    <a href="{{ route('painel.contatos.divisoes.emails.create', $divisao->id) }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar E-mail
    </a>
</legend>

@if(!count($emails))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="contatos_emails">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Título [PT]</th>
                <th scope="col">E-mail</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($emails as $email)
            <tr id="{{ $email->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>{{ $email->titulo_pt }}</td>
                <td>{{ $email->email }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.contatos.divisoes.emails.destroy', [$divisao->id, $email->id]],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.contatos.divisoes.emails.edit', [$divisao->id, $email->id] ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection