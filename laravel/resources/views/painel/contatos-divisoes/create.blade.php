@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CONTATOS - Divisões |</small> Adicionar Divisão</h2>
</legend>

{!! Form::open(['route' => 'painel.contatos.divisoes.store', 'files' => true]) !!}

@include('painel.contatos-divisoes.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection