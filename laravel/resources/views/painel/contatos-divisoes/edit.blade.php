@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CONTATOS - Divisões |</small> Editar Divisão</h2>
</legend>

{!! Form::model($contato, [
'route' => ['painel.contatos.divisoes.update', $contato->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.contatos-divisoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection