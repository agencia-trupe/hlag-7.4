<ul class="nav navbar-nav">

    <li>
        <a href="{{ route('painel.programacao.index') }}" class="nav-link px-3 @if(Tools::routeIs(['painel.programacao*', 'painel.portos*'])) active @endif">Programação</a>
    </li>

    <li>
        <a href="{{ route('painel.servicos.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.servicos*')) active @endif">Serviços</a>
    </li>

    <li>
        <a href="{{ route('painel.terminais-de-atracacao.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.terminais-de-atracacao*')) active @endif">Terminais de Atracação</a>
    </li>

    <li>
        <a href="{{ route('painel.contatos.divisoes.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.contatos.divisoes*')) active @endif">Contatos Divisões</a>
    </li>

    <li>
        <a href="{{ route('painel.contato.index') }}" class="dropdown-item @if(Tools::routeIs('painel.contato.index')) active @endif">Informações de contato</a>
    </li>

</ul>