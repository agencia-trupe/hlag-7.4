@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">SERVIÇOS</h2>

    <a href="{{ route('painel.servicos.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Serviço
    </a>
</legend>


@if(!count($servicos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="servicos">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Título [PT]</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($servicos as $servico)
            <tr id="{{ $servico->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    <a href="{{ Tools::parseLink($servico->link) }}" target="_blank">{{ $servico->titulo_pt }}</a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.servicos.destroy', $servico->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.servicos.edit', $servico->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection