@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>SERVIÇOS |</small> Editar Serviço</h2>
</legend>

{!! Form::model($servico, [
'route' => ['painel.servicos.update', $servico->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.servicos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection