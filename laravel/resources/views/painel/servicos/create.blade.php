@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>SERVIÇOS |</small> Adicionar Serviço</h2>
</legend>

{!! Form::open(['route' => 'painel.servicos.store', 'files' => true]) !!}

@include('painel.servicos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection