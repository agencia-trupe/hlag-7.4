@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<a href="{{ route('painel.programacao.index') }}" title="Voltar para Programação" class="btn btn-secondary mb-3 col-2" style="width: 25%; font-size:14px">
    &larr; Voltar para Programação</a>

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">PROGRAMAÇÃO | Histórico</h2>
</legend>

@if(!count($historico))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable">
        <thead>
            <tr>
                <th scope="col">Arquivo</th>
                <th scope="col">Enviado em</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($historico as $registro)
            <tr id="{{ $registro->id }}">
                <td>
                    <a href="{{ route('painel.programacao.historico.download', $registro->id) }}" target="_blank">
                        {{ $registro->arquivo }}
                    </a>
                </td>
                <td style="white-space:nowrap">{{ $registro->created_at->format('d/m/Y H:i:s') }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

{!! $historico->links() !!}
@endif

@endsection