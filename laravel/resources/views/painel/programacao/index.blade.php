@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">PROGRAMAÇÃO</h2>

    <div class="d-flex">
        <a href="{{ route('painel.programacao.historico') }}" class="btn btn-warning btn-sm d-flex align-items-center justify-content-center" style="height: 30px;">
            <i class="bi bi-clock-history me-2 mb-1"></i>
            Histórico
        </a>
        <a href="{{ route('painel.portos.index') }}" class="btn btn-info btn-sm d-flex align-items-center justify-content-center" style="height: 30px;">
            <i class="bi bi-pencil-square me-2 mb-1"></i>
            Editar Portos
        </a>
    </div>

</legend>

<h4>Importar Arquivo</h4>

<div class="mb-3">
    {!! Form::open(['route' => 'painel.programacao.importar', 'files' => true]) !!}
    <div class="input-group">
        {!! Form::file('arquivo', ['class' => 'form-control btn-input-file', 'required' => true]) !!}
        <span class="input-group-btn">
            {!! Form::submit('Enviar', ['class' => 'btn btn-success btn-input-file']) !!}
        </span>
    </div>
    {!! Form::close() !!}

    @if(count($programacao))
    <div class="alert alert-info" style="margin:10px 0 0">
        <i class="bi bi-info-circle-fill me-2 mb-1"></i>
        Ao importar um novo arquivo todos os registros abaixo serão substituídos.
    </div>
    @endif
</div>

@if(!count($programacao))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="d-flex align-items-center mt-4 mb-2">
    <a href="{{ route('painel.programacao.limpar') }}" class="btn btn-danger btn-voltar">
        <i class="bi bi-trash3 me-2"></i>
        Limpar Programação
    </a>
</div>

<div class="well">
    Divulgado em:
    <strong>{{ $programacao->last()->created_at->format('d/m/Y') }}</strong>
</div>

<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable tabela-programacao">
        <thead>
            <tr>
                <th scope="col">Porto</th>
                <th scope="col">Navio</th>
                <th scope="col">Viagem</th>
                <th scope="col">Serviço</th>
                <th scope="col">Previsão de Atracação</th>
                <th scope="col">Previsão de Saída</th>
                <th scope="col">Deadline de DCA</th>
                <th scope="col">Deadline de MDGF</th>
                <th scope="col">Deadline de Draft</th>
                <th scope="col">Deadline de Carga/Liberação</th>
                <th scope="col">VGM</th>
                <th scope="col">Terminal</th>
                <th scope="col">Pernada</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($programacao as $registro)
            <tr id="{{ $registro->id }}">
                <td>{{ $registro->porto }}</td>
                <td>{{ $registro->navio }}</td>
                <td>{{ $registro->viagem }}</td>
                <td>{{ $registro->servico }}</td>
                <td>{{ $registro->previsao_atracacao->format('d/m') }}</td>
                <td>{{ $registro->previsao_saida->format('d/m') }}</td>
                <td>{{ $registro->deadline_dca->format('d/m H:i') }}</td>
                <td>{{ $registro->deadline_mdgf->format('d/m H:i') }}</td>
                <td>{{ $registro->deadline_draft->format('d/m H:i') }}</td>
                <td>{{ $registro->deadline_carga_liberacao->format('d/m H:i') }}</td>
                <td>{{ $registro->deadline_vgm->format('d/m H:i') }}</td>
                <td>{{ $registro->terminal }}</td>
                <td>{{ $registro->pernada }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection