@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">CONTATO - Informações de Contato</h2>
</legend>

{!! Form::model($contato, [
'route' => ['painel.contato.update', $contato->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.contato.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection