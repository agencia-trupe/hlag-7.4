@include('painel.layout.flash')

<div class="row">
    <div class="mb-3 col-md-4">
        {!! Form::label('razao_social_pt', 'Razão Social [PT]') !!}
        {!! Form::textarea('razao_social_pt', null, ['class' => 'form-control editor-clean']) !!}
    </div>
    <div class="mb-3 col-md-4">
        {!! Form::label('razao_social_en', 'Razão Social [EN]') !!}
        {!! Form::textarea('razao_social_en', null, ['class' => 'form-control editor-clean']) !!}
    </div>
    <div class="mb-3 col-md-4">
        {!! Form::label('razao_social_es', 'Razão Social [ES]') !!}
        {!! Form::textarea('razao_social_es', null, ['class' => 'form-control editor-clean']) !!}
    </div>
</div>

<div class="row">
    <div class="mb-3 col-md-4">
        {!! Form::label('endereco_pt', 'Endereço [PT]') !!}
        {!! Form::textarea('endereco_pt', null, ['class' => 'form-control editor-clean']) !!}
    </div>
    <div class="mb-3 col-md-4">
        {!! Form::label('endereco_en', 'Endereço [EN]') !!}
        {!! Form::textarea('endereco_en', null, ['class' => 'form-control editor-clean']) !!}
    </div>
    <div class="mb-3 col-md-4">
        {!! Form::label('endereco_es', 'Endereço [ES]') !!}
        {!! Form::textarea('endereco_es', null, ['class' => 'form-control editor-clean']) !!}
    </div>
</div>

<div class="row">
    <div class="mb-3 col-md-4">
        {!! Form::label('telefone_pt', 'Telefone [PT]') !!}
        {!! Form::text('telefone_pt', null, ['class' => 'form-control input-text input-telefone']) !!}
    </div>
    <div class="mb-3 col-md-4">
        {!! Form::label('telefone_en', 'Telefone [EN]') !!}
        {!! Form::text('telefone_en', null, ['class' => 'form-control input-text input-telefone']) !!}
    </div>
    <div class="mb-3 col-md-4">
        {!! Form::label('telefone_es', 'Telefone [ES]') !!}
        {!! Form::text('telefone_es', null, ['class' => 'form-control input-text input-telefone']) !!}
    </div>
</div>

<div class="row">
    <div class="mb-3 col-md-4">
        {!! Form::label('aviso_pt', 'Aviso [PT]') !!}
        {!! Form::text('aviso_pt', null, ['class' => 'form-control input-text input-telefone']) !!}
    </div>
    <div class="mb-3 col-md-4">
        {!! Form::label('aviso_en', 'Aviso [ES]') !!}
        {!! Form::text('aviso_en', null, ['class' => 'form-control input-text input-telefone']) !!}
    </div>
    <div class="mb-3 col-md-4">
        {!! Form::label('aviso_es', 'Aviso [ES]') !!}
        {!! Form::text('aviso_es', null, ['class' => 'form-control input-text input-telefone']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}
</div>