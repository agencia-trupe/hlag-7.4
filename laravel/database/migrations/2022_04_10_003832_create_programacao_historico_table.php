<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramacaoHistoricoTable extends Migration
{
    public function up()
    {
        Schema::create('programacao_historico', function (Blueprint $table) {
            $table->id();
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('programacao_historico');
    }
}
