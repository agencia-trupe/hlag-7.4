<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTerminaisDeAtracacaoTable extends Migration
{
    public function up()
    {
        Schema::create('terminais_de_atracacao', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('terminais_de_atracacao');
    }
}
