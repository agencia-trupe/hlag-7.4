<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosEmailsTable extends Migration
{
    public function up()
    {
        Schema::create('contatos_emails', function (Blueprint $table) {
            $table->id();
            $table->foreignId('divisao_id')->nullable()->constrained('contatos_divisoes')->cascadeOnUpdate()->nullOnDelete();
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('email');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contatos_emails');
    }
}
