<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosDivisoesTable extends Migration
{
    public function up()
    {
        Schema::create('contatos_divisoes', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('telefones_pt');
            $table->string('telefones_en');
            $table->string('telefones_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contatos_divisoes');
    }
}
