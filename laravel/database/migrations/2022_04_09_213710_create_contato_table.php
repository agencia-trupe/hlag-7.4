<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatoTable extends Migration
{
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->id();
            $table->text('razao_social_pt');
            $table->text('razao_social_en');
            $table->text('razao_social_es');
            $table->text('endereco_pt');
            $table->text('endereco_en');
            $table->text('endereco_es');
            $table->string('telefone_pt');
            $table->string('telefone_en');
            $table->string('telefone_es');
            $table->string('aviso_pt');
            $table->string('aviso_en');
            $table->string('aviso_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contato');
    }
}
