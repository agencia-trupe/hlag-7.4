<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortosTable extends Migration
{
    public function up()
    {
        Schema::create('portos', function (Blueprint $table) {
            $table->id();
            $table->string('sigla')->unique();
            $table->string('titulo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('portos');
    }
}
