<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramacaoTable extends Migration
{
    public function up()
    {
        Schema::create('programacao', function (Blueprint $table) {
            $table->id();
            $table->string('porto');
            $table->string('navio');
            $table->string('viagem');
            $table->string('servico');
            $table->string('previsao_atracacao');
            $table->string('previsao_saida');
            $table->string('deadline_dca');
            $table->string('deadline_mdgf');
            $table->string('deadline_draft');
            $table->string('deadline_carga_liberacao');
            $table->string('deadline_vgm');
            $table->string('terminal');
            $table->string('pernada');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('programacao');
    }
}
