<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'titulo_pt' => 'Hapag-Lloyd',
            'titulo_en' => 'Hapag-Lloyd',
            'titulo_es' => 'Hapag-Lloyd',
            'link' => 'https://www.hapag-lloyd.com'
        ]);
    }
}
