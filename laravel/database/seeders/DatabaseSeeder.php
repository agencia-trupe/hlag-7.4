<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(ServicoTableSeeder::class);
        $this->call(TerminaisDeAtracacaoTableSeeder::class);
        $this->call(ContatosDivisoesTableSeeder::class);
        $this->call(ContatosEmailsTableSeeder::class);
        $this->call(PortosTableSeeder::class);
    }
}
