<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosEmailsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos_emails')->insert([
            'divisao_id' => 1,
            'titulo_pt' => 'Sales Support',
            'titulo_en' => 'Sales Support',
            'titulo_es' => 'Sales Support',
            'email' => 'sales.br@hlag.com',
        ]);
    }
}
