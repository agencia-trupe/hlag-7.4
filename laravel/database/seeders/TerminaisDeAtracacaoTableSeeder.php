<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TerminaisDeAtracacaoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('terminais_de_atracacao')->insert([
            'titulo_pt' => 'Hapag-Lloyd',
            'titulo_en' => 'Hapag-Lloyd',
            'titulo_es' => 'Hapag-Lloyd',
            'link' => 'https://www.hapag-lloyd.com'
        ]);
    }
}
