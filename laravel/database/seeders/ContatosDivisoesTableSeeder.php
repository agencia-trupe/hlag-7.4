<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosDivisoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos_divisoes')->insert([
            'titulo_pt' => 'Customer Service',
            'titulo_en' => 'Customer Service',
            'titulo_es' => 'Customer Service',
            'telefones_pt' => 'Phone 4090 1555 / 0300 11 55 300',
            'telefones_en' => 'Phone 4090 1555 / 0300 11 55 300',
            'telefones_es' => 'Phone 4090 1555 / 0300 11 55 300',
        ]);
    }
}
