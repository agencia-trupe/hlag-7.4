<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title' => 'Hapag-Lloyd',
            'description' => '',
            'keywords' => '',
            'imagem_de_compartilhamento' => '',
            'analytics' => '',
        ]);
    }
}
