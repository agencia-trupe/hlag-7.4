<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PortosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('portos')->insert([
            [
                'sigla' => 'BRSSZ',
                'titulo' => 'SANTOS',
            ],
            [
                'sigla' => 'BRRIO',
                'titulo' => 'RIO DE JANEIRO',
            ],
            [
                'sigla' => 'BRSSA',
                'titulo' => 'SALVADOR',
            ],
            [
                'sigla' => 'BRSRG',
                'titulo' => 'RIO GRANDE',
            ],

        ]);
    }
}
