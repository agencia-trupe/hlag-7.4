<?php

namespace App\Exports;

use App\Models\Programacao;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProgramacaoExport implements FromCollection, WithHeadings, WithStyles, WithEvents, WithMapping
{
    protected $resultados;

    function __construct(Collection $resultados)
    {
        $this->resultados = $resultados;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->resultados;
    }

    public function headings(): array
    {
        $campos = ['PORTO', 'NAVIO', 'VIAGEM', 'SERVIÇO', 'PREVISÃO DE ATRACAÇÃO', 'PREVISÃO DE SAÍDA', 'DEADLINE DE DCA', 'DEADLINE DE MDGF', 'DEADLINE DE DRAFT', 'DEADLINE DE CARGA/LIBER.', 'DEADLINE DE VGM', 'TERMINAL', 'PERNADA'];
        return $campos;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'size' => 9,
                    'bold' => true,
                    'color' => ['argb' => '23467d']
                ]
            ],
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {

                $event->sheet->getDelegate()->getStyle('A1:M1')
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('eeeeee');
            },
        ];
    }

    public function map($resultado): array
    {
        return [
            $resultado->porto,
            $resultado->navio,
            $resultado->viagem,
            $resultado->servico,
            $resultado->previsao_atracacao->format('d/m'),
            $resultado->previsao_saida->format('d/m'),
            $resultado->deadline_dca->format('d/m H:i'),
            $resultado->deadline_mdgf->format('d/m H:i'),
            $resultado->deadline_draft->format('d/m H:i'),
            $resultado->deadline_carga_liberacao->format('d/m H:i'),
            $resultado->deadline_vgm->format('d/m H:i'),
            $resultado->terminal,
            $resultado->pernada,
        ];
    }
}
