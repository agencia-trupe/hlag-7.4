<?php

namespace App\Providers;

use App\Models\Configuracao;
use App\Models\Contato;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('path.public', function () {
        //     return realpath(base_path() . '/../');
        // });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        Paginator::useBootstrap();

        View::composer('*', function ($view) {
            $view->with('config', Configuracao::first());
        });

        View::composer('frontend.*', function ($view) {
            $view->with('contato', Contato::first());
        });
    }
}
