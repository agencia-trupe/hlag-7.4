<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class Tools
{
    public static function routeIs($routeNames)
    {
        foreach ((array) $routeNames as $routeName) {
            if (Str::is($routeName, Route::currentRouteName())) {
                return true;
            }
        }

        return false;
    }

    public static function fileUpload($input, $path = 'arquivos/')
    {
        if (!$file = request()->file($input)) {
            throw new \Exception("O campo (${input}) não contém nenhum arquivo.", 1);
        }

        $fileName  = Str::slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $fileName .= '_' . date('YmdHis');
        $fileName .= Str::random(10);
        $fileName .= '.' . $file->getClientOriginalExtension();

        $file->move(public_path($path), $fileName);

        return $fileName;
    }

    public static function parseLink($url)
    {
        return parse_url($url, PHP_URL_SCHEME) === null ? 'http://' . $url : $url;
    }

    public static function formataData($data, $locale)
    {
        $meses = [
            'pt' => [
                'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'
            ],
            'en' => [
                'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'
            ]
        ];

        list($mes, $ano) = explode('/', $data);

        return $meses[$locale][(int) $mes - 1] . ' ' . $ano;
    }

    public static function str_words($value, $words = 100, $end = '...')
    {
        preg_match('/^\s*+(?:\S++\s*+){1,' . $words . '}/u', $value, $matches);

        if (!isset($matches[0])) return $value;

        if (strlen($value) == strlen($matches[0])) return $value;

        return rtrim($matches[0]) . $end;
    }

    public static function dataValidaQuery($data)
    {
        return preg_match(
            '/^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])$/',
            $data
        );
    }

    public static function formataDataQuery($data)
    {
        list($dia, $mes) = explode('/', $data);
        return $mes . '-' . $dia;
    }
}
