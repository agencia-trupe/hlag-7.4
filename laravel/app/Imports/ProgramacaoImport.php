<?php

namespace App\Imports;

use App\Models\Programacao;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithUpsertColumns;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
// use Maatwebsite\Excel\Concerns\OnEacRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ProgramacaoImport implements ToCollection, WithStartRow, WithHeadingRow, WithMapping
{
    public function startRow(): int
    {
        return 2;
    }

    public function map($row): array
    {
        return [
            'departure' => $row['departure'],
            'navio' => $row['navio'],
            'viagem' => $row['viagem'],
            'servico' => $row['servico'],
            'previsao_de_atracacao' => Date::excelToDateTimeObject($row['previsao_de_atracacao'])->format('d/m'),
            'previsao_de_saida' => Date::excelToDateTimeObject($row['previsao_de_saida'])->format('d/m'),
            'dl_de_dca' => Date::excelToDateTimeObject($row['dl_de_dca'])->format('d/m H:i'),
            'dl_de_mdgf' => Date::excelToDateTimeObject($row['dl_de_mdgf'])->format('d/m H:i'),
            'dl_de_draft' => Date::excelToDateTimeObject($row['dl_de_draft'])->format('d/m H:i'),
            'dl_de_cargaliberacao' => Date::excelToDateTimeObject($row['dl_de_cargaliberacao'])->format('d/m H:i'),
            'vgm' => Date::excelToDateTimeObject($row['vgm'])->format('d/m H:i'),
            'terminal' => $row['terminal'],
            'pernada' => mb_substr(ucfirst(strrev($row['viagem'])), 0, 1),
        ];
    }

    public function collection(Collection $results)
    {
        //
    }

    public function headingRow(): int
    {
        return 1;
    }
}
