<?php

namespace App\Models;

use App\Exports\ProgramacaoExport;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Dompdf\Dompdf;
use Dompdf\Options;
use Maatwebsite\Excel\Facades\Excel;

class ProgramacaoExportacao extends Model
{
    use HasFactory;

    protected $resultados;

    public function __construct(Collection $resultados)
    {
        $this->resultados = $resultados;
    }

    public function downloadPdf()
    {
        $data     = ['resultados' => $this->resultados];
        $fileName = 'HapagLloyd-Consulta-' . date('d-m-Y') . '.pdf';

        $options = new Options();
        $options->setChroot('');
        
        $dompdf = new Dompdf();
        $dompdf->setOptions($options);
        $dompdf->loadHtml(view('frontend.consulta-pdf', $data)->render());
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream($fileName);
    }
}
