<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Programacao extends Model
{
    use HasFactory;

    protected $table = 'programacao';

    protected $guarded = ['id'];

    protected $dates = [
        'previsao_atracacao',
        'previsao_saida',
        'deadline_dca',
        'deadline_mdgf',
        'deadline_draft',
        'deadline_carga_liberacao',
        'deadline_vgm',
    ];
}
