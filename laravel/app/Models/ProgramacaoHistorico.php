<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramacaoHistorico extends Model
{
    use HasFactory;
    
    protected $table = 'programacao_historico';

    protected $guarded = ['id'];
}
