<?php

namespace App\Models;

use App\Imports\ProgramacaoImport;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Collections\CellCollection;
use Maatwebsite\Excel\Collections\SheetCollection;

class ProgramacaoImportacao extends Model
{
    use HasFactory;

    private $arquivo;
    private $results;
    private $portos;

    private $campos = [
        'departure'                    => 'Departure',
        'navio'                    => 'Navio',
        'viagem'                   => 'Viagem',
        'servico'                  => 'Serviço',
        'previsao_de_atracacao'       => 'Previsão de Atracação',
        'previsao_de_saida'           => 'Previsão de Saída',
        'dl_de_dca'             => 'Deadline de DCA',
        'dl_de_mdgf'            => 'Deadline de MDGF',
        'dl_de_draft'           => 'Deadline de Draft',
        'dl_de_cargaliberacao' => 'Deadline de Carga/Liberação',
        'vgm'             => 'VGM',
        'terminal'                 => 'Terminal',
        'pernada'                  => 'Pernada',
    ];

    public function __construct($arquivo)
    {
        $this->arquivo = $arquivo;

        $this->results = Excel::toCollection(new ProgramacaoImport, $this->arquivo)->first();

        $this->portos = Porto::pluck('titulo', 'sigla')->toArray();
    }

    public function importar()
    {
        $erros = $this->validar();

        if (count($erros)) {
            throw new \App\Exceptions\ProgramacaoException(
                'O Arquivo contém erros de validação.',
                0,
                null,
                $erros
            );
        }

        $this->salvaHistorico();

        $novaProgramacao = $this->geraProgramacao();

        $this->atualizaProgramacao($novaProgramacao);
    }

    private function validar()
    {
        $erros = [];

        foreach ($this->results as $rowNum => $row) {

            if ($this->linhaVazia($row)) continue;

            $rowNum = $rowNum + 2;

            $erros  = array_merge(
                $erros,
                $this->validaCamposObrigatorios($row, $rowNum),
                $this->validaPortoExistente($row, $rowNum),
                $this->validaFormatosDatas($row, $rowNum)
            );
        }
        return $erros;
    }

    private function linhaVazia($row)
    {
        foreach ($this->campos as $campo => $titulo) {
            if ($row[$campo] != null) {
                return false;
            }
        }
        return true;
    }

    private function validaCamposObrigatorios($row, $rowNum)
    {
        $erros = [];
        foreach ($this->campos as $campo => $titulo) {
            if (!$row[$campo] || $row[$campo] == '') {
                $erros[] = "Linha $rowNum - <strong>$titulo</strong> não preenchido(a).";
            }
        }
        return $erros;
    }

    private function validaPortoExistente($row, $rowNum)
    {
        $link = route('painel.portos.index');
        $porto = $row['departure'];

        if (!array_key_exists($porto, $this->portos)) {
            return [
                "Linha $rowNum - O porto <strong>$porto</strong> não existe. <a href=\"$link\" style=\"color:#fff;text-decoration:underline\">Cadastre-o aqui</a>."
            ];
        }
        return [];
    }

    private function validaFormatosDatas($row, $rowNum)
    {
        $erros = [];

        foreach ([
            'previsao_de_atracacao',
            'previsao_de_saida'
        ] as $campo) {
            if (!preg_match(
                '/^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])$/', // dd/mm
                $row[$campo]
            )) {
                $titulo = $this->campos[$campo];
                $erros[] = "Linha $rowNum - <strong>$titulo</strong>: formato de data inválido (formato deve ser: dd/mm).";
            }
        }
        foreach ([
            'dl_de_dca',
            'dl_de_mdgf',
            'dl_de_draft',
            'dl_de_cargaliberacao',
            'vgm',
        ] as $campo) {
            if (!preg_match(
                '/^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2]) (0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', // dd/mm HH:ii
                $row[$campo]
            )) {
                $titulo = $this->campos[$campo];
                $erros[] = "Linha $rowNum - <strong>$titulo</strong>: formato de data inválido (formato deve ser: dd/mm HH:ii).";
            }
        }
        return $erros;
    }

    private function uploadHistorico()
    {
        $arquivo = $this->arquivo;

        $fileName  = Str::slug(
            pathinfo($arquivo->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $fileName .= '_' . date('YmdHis');
        $fileName .= Str::random(10);
        $fileName .= '.' . $arquivo->getClientOriginalExtension();

        $arquivo->move(storage_path('app/historico-programacao'), $fileName);

        return $fileName;
    }

    private function salvaHistorico()
    {
        $arquivo = $this->uploadHistorico();
        ProgramacaoHistorico::create(['arquivo' => $arquivo]);
    }

    private function geraProgramacao()
    {
        $programacao = [];
        $now         = Carbon::now();

        foreach ($this->results as $row) {

            if ($this->linhaVazia($row)) continue;

            $programacao[] = [
                'porto'                    => $row['departure'],
                'navio'                    => $row['navio'],
                'viagem'                   => $row['viagem'],
                'servico'                  => $row['servico'],
                'deadline_vgm'             => $row['vgm'],
                'previsao_atracacao'       => Carbon::createFromFormat('d/m', $row['previsao_de_atracacao']),
                'previsao_saida'           => Carbon::createFromFormat('d/m', $row['previsao_de_saida']),
                'deadline_dca'             => Carbon::createFromFormat('d/m H:i', $row['dl_de_dca']),
                'deadline_mdgf'            => Carbon::createFromFormat('d/m H:i', $row['dl_de_mdgf']),
                'deadline_draft'           => Carbon::createFromFormat('d/m H:i', $row['dl_de_draft']),
                'deadline_carga_liberacao' => Carbon::createFromFormat('d/m H:i', $row['dl_de_cargaliberacao']),
                'deadline_vgm'             => Carbon::createFromFormat('d/m H:i', $row['vgm']),
                'terminal'                 => $row['terminal'],
                'pernada'                  => $row['pernada'],
                'created_at'               => $now,
                'updated_at'               => $now
            ];
        }

        return $programacao;
    }

    private function atualizaProgramacao(array $programacao)
    {
        Programacao::truncate();
        // Programacao::insert($programacao);
        // Fazendo inserções individuais para evitar erro SQL Server de limite de campos por transação
        foreach ($programacao as $p) Programacao::insert($p);
    }
}
