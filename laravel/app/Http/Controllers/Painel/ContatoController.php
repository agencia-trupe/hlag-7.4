<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContatoRequest;
use App\Models\Contato;
use Illuminate\Http\Request;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('painel.contato.edit', compact('contato'));
    }

    public function update(ContatoRequest $request, Contato $contato)
    {
        try {
            $input = $request->all();
            $contato->update($input);

            return redirect()->route('painel.contato.index')->with('success', 'Informações alteradas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar informações: ' . $e->getMessage()]);
        }
    }
}
