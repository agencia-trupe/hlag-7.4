<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContatosEmailsRequest;
use App\Models\ContatoDivisao;
use App\Models\ContatoEmail;
use Illuminate\Http\Request;

class ContatosEmailsController extends Controller
{
    public function index(ContatoDivisao $diviso)
    {
        $divisao = $diviso;
        $emails = $divisao->emails;

        return view('painel.contatos-divisoes.emails.index', compact('divisao', 'emails'));
    }

    public function create(ContatoDivisao $diviso)
    {
        $divisao = $diviso;
        return view('painel.contatos-divisoes.emails.create', compact('divisao'));
    }

    public function store(ContatosEmailsRequest $request, ContatoDivisao $diviso)
    {
        try {
            $input = $request->all();

            $diviso->emails()->create($input);

            return redirect()->route('painel.contatos.divisoes.emails.index', $diviso->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ContatoDivisao $diviso, ContatoEmail $email)
    {
        $divisao = $diviso;
        return view('painel.contatos-divisoes.emails.edit', compact('divisao', 'email'));
    }

    public function update(ContatosEmailsRequest $request, ContatoDivisao $diviso, ContatoEmail $email)
    {
        try {
            $input = $request->all();

            $email->update($input);

            return redirect()->route('painel.contatos.divisoes.emails.index', $diviso->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ContatoDivisao $diviso, ContatoEmail $email)
    {
        try {
            $email->delete();

            return redirect()->route('painel.contatos.divisoes.emails.index', $diviso->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
