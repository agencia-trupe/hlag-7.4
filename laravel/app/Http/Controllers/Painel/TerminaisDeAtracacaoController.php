<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\TerminaisDeAtracacaoRequest;
use App\Models\Terminal;
use Illuminate\Http\Request;

class TerminaisDeAtracacaoController extends Controller
{
    public function index()
    {
        $terminais = Terminal::ordenados()->get();

        return view('painel.terminais-de-atracacao.index', compact('terminais'));
    }

    public function create()
    {
        return view('painel.terminais-de-atracacao.create');
    }

    public function store(TerminaisDeAtracacaoRequest $request)
    {
        try {
            $input = $request->all();

            Terminal::create($input);

            return redirect()->route('painel.terminais-de-atracacao.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Terminal $terminais_de_atracacao)
    {
        $terminal = $terminais_de_atracacao;
        return view('painel.terminais-de-atracacao.edit', compact('terminal'));
    }

    public function update(TerminaisDeAtracacaoRequest $request, Terminal $terminais_de_atracacao)
    {
        try {
            $input = $request->all();

            $terminais_de_atracacao->update($input);

            return redirect()->route('painel.terminais-de-atracacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Terminal $terminais_de_atracacao)
    {
        try {
            $terminais_de_atracacao->delete();

            return redirect()->route('painel.terminais-de-atracacao.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
