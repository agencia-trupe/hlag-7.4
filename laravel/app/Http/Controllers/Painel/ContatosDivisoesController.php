<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContatosDivisoesRequest;
use App\Models\ContatoDivisao;
use Illuminate\Http\Request;

class ContatosDivisoesController extends Controller
{
    public function index()
    {
        $contatos = ContatoDivisao::ordenados()->get();

        return view('painel.contatos-divisoes.index', compact('contatos'));
    }

    public function create()
    {
        return view('painel.contatos-divisoes.create');
    }

    public function store(ContatosDivisoesRequest $request)
    {
        try {
            $input = $request->all();

            ContatoDivisao::create($input);

            return redirect()->route('painel.contatos.divisoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ContatoDivisao $diviso)
    {
        $contato = $diviso;
        return view('painel.contatos-divisoes.edit', compact('contato'));
    }

    public function update(ContatosDivisoesRequest $request, ContatoDivisao $diviso)
    {
        try {
            $input = $request->all();

            $diviso->update($input);

            return redirect()->route('painel.contatos.divisoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ContatoDivisao $diviso)
    {
        try {
            $diviso->delete();

            return redirect()->route('painel.contatos.divisoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
