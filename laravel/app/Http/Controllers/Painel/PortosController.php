<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\PortosRequest;
use App\Models\Porto;
use Illuminate\Http\Request;

class PortosController extends Controller
{
    public function index()
    {
        $portos = Porto::get();

        return view('painel.portos.index', compact('portos'));
    }

    public function create()
    {
        return view('painel.portos.create');
    }

    public function store(PortosRequest $request)
    {
        try {
            $input = $request->all();

            Porto::create($input);

            return redirect()->route('painel.portos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Porto $porto)
    {
        return view('painel.portos.edit', compact('porto'));
    }

    public function update(PortosRequest $request, Porto $porto)
    {
        try {
            $input = $request->all();

            $porto->update($input);

            return redirect()->route('painel.portos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Porto $porto)
    {
        try {
            $porto->delete();

            return redirect()->route('painel.portos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
