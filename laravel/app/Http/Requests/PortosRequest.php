<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class PortosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currentId = Route::current()->portos
            ? Route::current()->portos->id
            : null;

        $rules = [
            'sigla' => 'required|unique:portos,sigla,' . $currentId,
            'titulo' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
