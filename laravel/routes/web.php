<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('painel', function() {
    return redirect()->route('painel');
});

Route::get('/', function() {
    return redirect()->route('home', app()->getLocale());
});

Route::get('{lang}', [HomeController::class, 'index'])->name('home');
Route::get('{lang}/consult', [HomeController::class, 'consulta'])->name('consulta');
Route::get('{lang}/consult/{extension}', [HomeController::class, 'consultaExportacao'])->name('consulta.exportacao');
Route::get('{lang}/services', [HomeController::class, 'servicos'])->name('servicos');
Route::get('{lang}/ship-programming', [HomeController::class, 'terminais'])->name('terminais');
Route::get('{lang}/contacts', [HomeController::class, 'contatos'])->name('contatos');

// LANG
Route::get('lang/{idioma?}', function ($idioma = 'pt') {
    if (in_array($idioma, ['pt', 'en', 'es'])) {
        Session::put('locale', $idioma);
        App::setLocale($idioma);
    }
    return redirect()->route('home', $idioma);
})->name('lang');

require __DIR__ . '/auth.php';
