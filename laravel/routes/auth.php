<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Painel\ConfiguracoesController;
use App\Http\Controllers\Painel\ContatoController;
use App\Http\Controllers\Painel\ContatosDivisoesController;
use App\Http\Controllers\Painel\ContatosEmailsController;
use App\Http\Controllers\Painel\PainelController;
use App\Http\Controllers\Painel\PortosController;
use App\Http\Controllers\Painel\ProgramacaoController;
use App\Http\Controllers\Painel\ServicosController;
use App\Http\Controllers\Painel\TerminaisDeAtracacaoController;
use App\Http\Controllers\Painel\UsersController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'painel'
], function () {
    // AUTH - BREEZE
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest')
        ->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest');
    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest')
        ->name('password.request');
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.email');
    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest')
        ->name('password.reset');
    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');
    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth')
        ->name('password.confirm');
    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth');
    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth')
        ->name('logout');


    // PAINEL
    Route::group([
        'middleware' => ['auth']
    ], function () {
        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('cache:clear');

            return 'DONE';
        });

        Route::get('/home', [PainelController::class, 'index'])->name('painel');
        Route::post('image-upload', [PainelController::class, 'imageUpload']);
        Route::post('order', [PainelController::class, 'order']);

        Route::name('painel.')->group(function () {
            Route::get('programacao', [ProgramacaoController::class, 'index'])->name('programacao.index');
            Route::post('programacao/importar', [ProgramacaoController::class, 'importar'])->name('programacao.importar');
            Route::get('programacao/limpar', [ProgramacaoController::class, 'limpar'])->name('programacao.limpar');
            Route::get('programacao/historico', [ProgramacaoController::class, 'historico'])->name('programacao.historico');
            Route::get('programacao/historico/{historico}/download', [ProgramacaoController::class, 'historicoDownload'])->name('programacao.historico.download');
            Route::resource('portos', PortosController::class)->except('show');

            Route::resource('usuarios', UsersController::class)->except('show');
            Route::resource('contato', ContatoController::class)->only(['index', 'update']);
            Route::resource('configuracoes', ConfiguracoesController::class)->only(['index', 'update']);
            Route::resource('servicos', ServicosController::class)->except('show');
            Route::resource('terminais-de-atracacao', TerminaisDeAtracacaoController::class)->except('show');
            Route::resource('contatos/divisoes', ContatosDivisoesController::class)->except('show')->names('contatos.divisoes');
            Route::resource('contatos/divisoes.emails', ContatosEmailsController::class)->except('show')->names('contatos.divisoes.emails');
        });
    });
});
